/*
 *  Author: Paul Horton
 *  Created: 20221007
 *  Copyright: Paul Horton 2022
 *  License: GPLv3
 *
 *  Compile: gcc  -O2 -Wall  -o fastafmt_dump_seqLengths  ../../libs/fastafmt.c fastafmt_dump_seqLengths.c
 *
 *  Try me:  ./fastafmt_dump_seqLengths < ../../example_input_files/proteins.fasta1
 *
 *  Description:  Output summary of information in fastafmt records.
 */
#include <sysexits.h>
#include <unistd.h>
#include <getopt.h>
#include "../../libs/cksum_adler32.h"
#include "../../libs/fastafmt.h"

#define DIE_USAGE()   printf( "%s\n", usage ); exit(  EX_USAGE )



int main(  int argc,  char* argv[]  ){

  bool opt_dumpHeadP= false;
  FILE* ifp = stdin;

  {// ──────────  Parse argv  ──────────
    char usage[]= "Usage: %s [--head] [fastaSequenceFile]\n"
      " Default\n"
      "   outputs:   NAME <tab> LENGTH\n"
      " with --head option\n"
      "   outputs:   LENGTH <tab> HEAD\n"
      ;// END usage


    int argTop= 1;
    if(  1 < argc  &&  !strncmp( argv[argTop], "--head", 7)  ){
      opt_dumpHeadP= true;  ++argTop;
    }

    if( argTop < argc ){
      if( argTop+1 < argc ){
        DIE_USAGE();
      }
      char* finalArg=  argv[argTop];
      if(  finalArg[0] == '-'  ){
        if( finalArg[1] ){
          DIE_USAGE();
        }
        // else OK.  finalArg "-" means to use read from stdin.
      }
      else{//  finalArg does not start with "-"
        if(  !(ifp= fopen( finalArg, "r"))  ){
          printf(  "Could not open file '%s'", argv[argTop]  );
          exit(  EX_NOINPUT  );
        }
      }
    }
  }


  char rec_name[63];

  struct fastafmt_record* rec=  fastafmt_record1;

  while(  fastafmt_load_record1( ifp )  ){

    if(  opt_dumpHeadP  ){
      printf(  "%zu\t%s\n",  rec->seqLen, rec->head  );
    }
    else{
      fastafmt_copy_record1_name(  &rec_name[0], sizeof(rec_name)  );
      printf(  "%s\t%zu\n",  rec_name, rec->seqLen  );
    }
  }
}



