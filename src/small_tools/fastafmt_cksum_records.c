/*
 *  Author: Paul Horton
 *  Created: 20221007
 *  Copyright: Paul Horton 2022
 *  License: GPLv3
 *
 *  Compile: gcc  -O2 -Wall  -o fastafmt_cksum_records  ../../libs/cksum_adler32.c ../../libs/fastafmt.c fastafmt_cksum_records.c
 *
 *  Try me:  ./fastafmt_cksum_records < ../../example_input_files/proteins.fasta1
 *
 *  Description:  Output summary of information in fastafmt records.
 */
#include <sysexits.h>
#include <unistd.h>
#include "../../libs/cksum_adler32.h"
#include "../../libs/fastafmt.h"



int main(  int argc,  char* argv[]  ){

  if(   isatty( fileno(stdin))  &&  argc > 0   ){
    printf(  "Usage error.  No arguments expected; pipe fasta format data in from stdin\n" );
    exit(  EX_USAGE );
  }


  char name[63];

  struct fastafmt_record* rec=  fastafmt_record1;


  // print header line at top.
  // To consider: add an option to omit the header.
  printf(  "%-30s %-10s %s\n",  "name",  "head_cksum", "seq_cksum"  );

  while(  fastafmt_load_record1( stdin )  ){

    fastafmt_copy_record1_name(  &name[0], sizeof(name)  );

    size_t  head_cksum=
      (size_t) cksum_adler32(  (unsigned char*) rec->head,  strlen(rec->head)  );

    size_t   seq_cksum=
      (size_t) cksum_adler32(  (unsigned char*) rec->seq,   rec->seqLen        );

    printf(  "%-30s %10zu %10zu\n",  name,  head_cksum, seq_cksum  );
  }
}
