/*
 *  Author: Paul Horton
 *  Copyright: Paul Horton 2020,2022
 *  License: GNU General Public License GPLv3
 *  Created: 20200520
 *  Updated: 20221108
 *
 *  Description:  Program to count DNA dimers and indicate how enriched/depleted they are.
 *
 *  Compile:  gcc -O2 -Wall -Wno-char-subscripts -o fastafmt_countDNA3mers  ../../libs/fastafmt.c ../../libs/base_serialNumber_mapping.c  fastafmt_countDNA3mers.c -lm
 *
 *  !!!UNFINISHED PROGRAM UNDER CONSTRUCTION!!!
 *
 *  When finished, this program will count valid 3mers, 2mers and 1mers.
 *  ...
 *  A valid 2mer consists of 2 characters both with _charToIdx values ≠ 127.
 *  So for example N's may be excluded.
 *  A base may be part of at most two valid 2mers.
 *  This program only counts bases which are part of one or two valid 2mers exactly once.
 *
 *  For example in the sequence "AANTNGGA".
 *  The forward strand monomer counts would be A3,G2: AA1,GA1,GG1
 *  the T between the N's does not get counted at all because it is not in a valid dimer.
 *  The counts of both strands combined (the default) would be A3,C2,G2,T3: AA1,CC1,GA1,GG1,TC1,TT1
 *
 *  Note that defined this way the 1mer counts cannot be inferred from the 2mer counts.
 *  For example seq:"AANAA" has 2 AA dimers covering 4 A's; while seq:"AAA" also has 2 AA dimers, but covers only 3 A's.
 *
 */
#define _GNU_SOURCE
#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include "../../libs/fastafmt.h"
#include "../../libs/base_serialNumber_mapping.h"


const baseSN_t num1mers=  DNA_baseSN_numBases;


// Arrays to kmer counts.
size_t freqs1[DNA_baseSN_numBases]= {0};

size_t freqs2[DNA_baseSN_numBases][DNA_baseSN_numBases]= {0};

size_t freqs3[DNA_baseSN_numBases][DNA_baseSN_numBases][DNA_baseSN_numBases]= {0};




void tally2mers(  const char* const seqBeg  );

#if 0
//  Add 3mers in range starting at seqBeg, terminated by SEQ_END char, to freqs2.
//  Also add: the first 1mer of any run of valid bases
//        and the first 2mer of any run of 2 or more valid bases.
void tally3mers(  const char* const seqBeg  ){
  const char* s=  seqBeg;
  baseSN_t base1, base2, base3;


 NEXT_VALID_3MER_RUN:
  {
    NEXT_VALID_2MER_RUN:
    //   Loop until base1 and base2 both hold valid bases
    //   But return early if SEQEND encountered.
    do{
      //  Skip initial run of NONBASEs
      while(   nonbaseP(  base1= baseSN_of_DNAchar( *s++ ))  )

      if(  seqendP(base1)  )   return;

      ++freqs1[base1];//  First base in run of one or more valid bases.

      base2= baseSN_of_DNAchar( *s++ );

      if(  seqendP(base1)  )   return;
    }
    while(  nonBaseP( base2 )  );

    ++freqs2[base1][base2];

    base3= baseSN_of_DNAchar( *s++ );

    if( endseqP(base3)


    for(;;){
      //  ASSERT: base1 one holds a valid base, and base2 holds its downstream base or NONBASE|SEQ_END
      if(  base2 >= DNA_baseSN_nonbase  ){
        if(  base2 == DNA_baseSN_seqEnd  )   return;
        goto  NEXT_VALID_2MER_RUN;
      }

      ++freqs2[base1][base2];	//freq2_inc( base1, base2 );

      //  Now swap roles of base1, and base2
      //  Consider base2 the upstream base and place the next base in base1.
      base1=  baseSN_of_DNAchar( *s++ );

      if(  base1 >= DNA_baseSN_nonbase  ){
        if(  base1 == DNA_baseSN_seqEnd  )   return;
        goto  NEXT_VALID_2MER_RUN;
      }


      ++freqs2[base2][base1];	//freq2_inc( base2, base1 );

      base2=  baseSN_of_DNAchar( *s++ );
    }
  }
}
#endif


int main(  const int argc,  const char* const argv[]  ){


  // ━━━━━━━━━━  Process Optional Flags  ━━━━━━━━━━
  bool tallyBothStrandsP= true;

  int argTop= 1;
  if(  argc > argTop  ){
    if(  !strncmp( argv[argTop], "-1", 3)  ){
      tallyBothStrandsP= false;  ++argTop;
    }
  }


  FILE* ifp = stdin;
  if( argc > argTop ){
    if(  !(ifp= fopen( argv[argTop++], "r"))  ){
        printf(  "Could not open file '%s'", argv[argTop]  );
        exit(  EX_NOINPUT  );
      }
  }


  if( argc > argTop ){
    printf(  "Usage: %s [-1] [fastaSequenceFile]\n\n"
             "-1 to tally only forward strand\n"
             "stdin used when fastaSequenceFile not given.\n"
             ,argv[0]  );
    exit(  EX_USAGE  );
  }


  // ━━━━━━━━━━  Read each record and add in its forward strand 2mer counts  ━━━━━━━━━━
  struct fastafmt_record* rec=  fastafmt_record1;

  while(  fastafmt_load_record1( ifp )  ){
    tally2mers(  rec->seq  );
  }



  // ━━━━━━━━━━  Compute marginal counts from 2mer counts  ━━━━━━━━━━
  size_t freqTot2= 0;  // total count of valid 2mers.  例: s=agxxtac has three {ag,ta,ac} valid 2mers

  //  Add 1mers counts from the downstream half of valid 2mers.
  //  For example, for single sequence s=𝚊𝚐𝙽𝙽𝚝𝚊𝚌, we add the 𝚐 of 𝚊𝚐, the 𝚊 of 𝚝𝚊, and the 𝚌 of 𝚊𝚌.
  for(  size_t base2= 0;  base2 < num1mers;  ++base2  ){
    size_t base2_sum= 0;
    for(  size_t base1= 0;  base1 < num1mers;  ++base1  )    base2_sum+= freqs2[base1][base2];
    freqs1[base2] += base2_sum;
    freqTot2 += base2_sum;
  }
  size_t freqTot1=  freqs1[0] + freqs1[1] + freqs1[2] + freqs1[3];



  if(  tallyBothStrandsP  ){
    // ━━━━━━━━━━  Add counts from reverse complement  ━━━━━━━━━━
    freqTot1 *= 2;  freqTot2 *= 2;

    {
      size_t freqs1Copy[DNA_baseSN_numBases];
      memcpy( freqs1Copy, freqs1, sizeof(freqs1Copy) );

      for(  baseSN_t base= 0;  base < num1mers;  ++base  ){
        freqs1[baseSNcompl_of_baseSN(base)] += freqs1Copy[base];
      }
    }

    {
      size_t freqs2Copy[DNA_baseSN_numBases][DNA_baseSN_numBases];
      memcpy(  freqs2Copy,  freqs2,  sizeof(freqs2)  );

      for(    baseSN_t base1= 0;  base1 < num1mers;  ++base1  ){
        for(  baseSN_t base2= 0;  base2 < num1mers;  ++base2  ){
          baseSN_t cmpl1=  baseSNcompl_of_baseSN(base1);
          baseSN_t cmpl2=  baseSNcompl_of_baseSN(base2);
          freqs2[base1][base2] += freqs2Copy[cmpl2][cmpl1];
        }
      }
    }
  }


  // ━━━━━━━━━━  Compute and display statistics  ━━━━━━━━━━
  const double pseudo_count=  0.5;//  Jeffreys Prior for multinomial distribution.

  printf( "total 1mers in valid 2mers: %10zu\n", freqTot1 );
  double pseudo_total=   (double) freqTot1  +  pseudo_count * num1mers;
  double probs1[num1mers];
  for(  baseSN_t base= 0;  base < num1mers;  ++base  ){
    probs1[base]=  (pseudo_count + freqs1[base]) / pseudo_total;
    printf(  "%c: %9zu%7.4f\n",  char_of_DNAbaseSN(base), freqs1[base], probs1[base]  );
  }

  printf(  "total 2mers counted: %10zu\n",  freqTot2  );
  printf(  "BB: frequency  prop.  lg odds\n"  );
  pseudo_total=  (double) freqTot2  +  pseudo_count * num2mers;
  for(    int base1= 0;  base1 < num1mers;  ++base1  ){
    for(  int base2= 0;  base2 < num1mers;  ++base2  ){
      // When counting both strands, just print one each for reverse complement pairs such as (AC,GT) or (AG,CT) etc.
      if(  !tallyBothStrandsP  ||  (base1 <= baseSNcompl_of_baseSN(base2))  ||  baseSNs_complP( base1, base2)  ){
        size_t diFreq=  freqs2[base1][base2];
        double diProb=  (diFreq + pseudo_count) / pseudo_total;
        printf(  "%c%c: %9zu %7.4f %7.4f\n",
                 char_of_DNAbaseSN(base1), char_of_DNAbaseSN(base2), diFreq,
                 diProb,
                 log2(diProb) - log2(probs1[base1]) - log2(probs1[base2])
                 );
        }
      }
  }
}



bool nonbaseP( baseSN_t base ){   return  base == DNA_baseSN_nonbase;   }


#define getNextSeqBaseSN_or_return(base)  ({base = baseSN_of_DNAchar( *seq++ );  if( base == DNA_baseSN_seqEnd ) return;  base;})


//  Add 2mers in range starting at seqBeg, terminated by SEQ_END char, to freqs2.
//  Also add the first 1mer of any run of valid bases to freqs1
void tally2mers(  const char* const seqBeg  ){
  const char* seq=  seqBeg;
  baseSN_t base1, base2;


 NEXT_VALID_2MER_RUN:
  {
  //   Loop until base1 holds a valid base and base2 the following base, or SEQ_END is encountered,
    do{

      //  Skip initial run of NONBASEs
      while(   DNA_baseSN_nonbase  ==  getNextSeqBaseSN_or_return(base1)   );

      ++freqs1[ base1 ];//  First base in run of one or more valid bases.

      getNextSeqBaseSN_or_return(base2);
    }
    while(  nonbaseP( base2 )  );

    for(;;){
      //  ASSERT: base1 one holds a valid base, and base2 holds its downstream base or NONBASE|SEQ_END
      if(  base2 >= DNA_baseSN_nonbase  ){
        goto  NEXT_VALID_2MER_RUN;
      }

      ++freqs2[base1][base2];	//freq2_inc( base1, base2 );

      //  Now swap roles of base1, and base2
      //  Consider base2 the upstream base and place the next base in base1.
      getNextSeqBaseSN_or_return(base1);

      if(  base1 >= DNA_baseSN_nonbase  ){
        goto  NEXT_VALID_2MER_RUN;
      }

      ++freqs2[base2][base1];	//freq2_inc( base2, base1 );

      getNextSeqBaseSN_or_return(base2);
    }
  }

}
