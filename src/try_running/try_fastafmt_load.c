/*
 *  Author: Paul Horton
 *  Copyright: Paul Horton 2022
 *  Created: 20221007
 *  License:  GPLv3
 *
 *  Compile: gcc  -O2 -Wall -o try_fastafmt_load ../../libs/fastafmt.c try_fastafmt_load.c
 *
 *  Try run: ./try_fastafmt_load < ../../example_input_files/smallSeqs2_comments.fasta
 *
 *  Description:  Small program to try out fastafmt load record function
 */
#include <sysexits.h>
#include <unistd.h>
#include "../../libs/fastafmt.h"



int main(  int argc,  char* argv[]  ){

  if(   isatty( fileno(stdin))  &&  argc > 0   ){
    printf(  "Usage error.  No arguments expected; pipe fasta format data in from stdin\n" );
    exit(  EX_USAGE );
  }


  struct fastafmt_record* rec=  fastafmt_record1;

  while(  fastafmt_load_record1( stdin )  ){
    printf(  "head is '%s'\n"   , rec->head    );
    printf(  "seq len = %zu\n"  , rec->seqLen  );
    printf(  "seq is:%s\n"      , rec->seq     );
  }

}

