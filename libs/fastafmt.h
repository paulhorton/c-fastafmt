/*
 *  Author: Paul Horton
 *  Copyright: Paul Horton 2022, All rights reserved.
 *  Created: 20221007
 *
 *  License:  GNU General Public License GPLv3
 *
 *  Description:  Provides easy way to reading from fasta format streams.
 *
 *  Limitations:  Only supports '\n' as end of line.
 *
 *  Features:  Limited Support for #line comments in fasta file (they are quietly skipped).
 *
 *  To do:  Allow customization of seqBuf initial size.
 *
 *  To consider:  ・ Extend to also be able to read fastq format files
 *                ・ Support utf8 encoded text in head line
 */
#pragma once
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



struct fastafmt_record{
  char*  seq;         //  C-string holding sequence, sans newlines
  size_t seqBufSize;  //  Size of memory seq points to.
  size_t seqLen;      //  Length of the sequence.

  char*  head;        //  C-string holding head line, sans the leading '>' char
  size_t headBufSize; //  Size of memory head points to.

};


//  fastafmt handles memory allocation for these two records.
//  I expect most programs using this library will use them.
extern  struct fastafmt_record*  fastafmt_record1;
extern  struct fastafmt_record*  fastafmt_record2;

extern  size_t  fastafmt_num_records_loaded;

/*  ────────────────────  Name Related  ────────────────────  */
//  In Perl regular expression terms, the name is captured by
//      $head =~/^\s*(\S*)/
//
//  Where \s means {space or tab} and \S means not {space or tab}
//
//  Note that the '>' character at the start of fasta format records
//  is not considered part of the record "head".
//
//  Unlike the head line and sequence; record names are not stored in struct fastafmt_record,
//  but instead extracted from head when needed.


//  Copy name part of head into caller provided buffer nameBuf as a C-string
//  If the name is longer than bufSize-1, it is truncated to bufSize-1 and bufSize is returned.
//  Otherwise the number of bytes in the name (including the terminating '\0') is returned.
size_t fastafmt_copy_name(  char* const nameBuf,  size_t bufSize,
                            const char* const head  );

//  Copy names of fastafmt_record1 or fastafmt_record2.
size_t fastafmt_copy_record1_name(  char* const nameBuf,  size_t bufSize  );
size_t fastafmt_copy_record2_name(  char* const nameBuf,  size_t bufSize  );


//  Set half-open interval [beg, end) to pointer to the name part of head.
//  head should be a null terminated C-string.
//
//  Used to implement fastafmt_copy_name.
//
//  When called like:
//     const char  *nameBeg, *nameEnd;
//     headString=  " seq1  and some more text to be ignored"
//     fastafmt_name_interval( &nameBeg, &nameEnd, headString )
//     nameBeg should point to the 's' of seq1, and nameEnd one past the '1'.
//
//  Returns the number of characters in the name.
//  Currently equivalent to end-beg  (but could differ if utf8 were supported).
size_t fastafmt_name_interval(   const char** beg,  const char** end,
                                 const char* const head  );



/*  ────────────────────  Loading Related  ────────────────────  */
// Read fasta record from ifp and load its information in
// Global fastafmt_record1, or fastafmt_record2
// overwriting its content.
//
// Return values:
//   0   No record read in, because no records remained in the stream
//   1   Final record read in stream read in.
//   2   Record read in, but it was not the final record.
int  fastafmt_load_record1(  FILE* ifp  );
int  fastafmt_load_record2(  FILE* ifp  );

//  Must pass in a pointer to fastafmt_record1 or fastafmt_record2.
int  fastafmt_load_record(  struct fastafmt_record* record1_or_2,  FILE* ifp  );


//  Write record REC to output stream OFP,
//  with sequence lines of length seqLineLen (except usually the final line)
void fastafmt_write_record1(  FILE* ofp,  size_t seqLineLen  );
void fastafmt_write_record2(  FILE* ofp,  size_t seqLineLen  );

void fastafmt_write(  const char* head,  const char* seq,
                      FILE* ofp,  size_t seqLineLen  );



size_t fastafmt_copy_record1_name(  char* const nameBuf,  size_t bufSize  );
size_t fastafmt_copy_record2_name(  char* const nameBuf,  size_t bufSize  );

//  Free memory held by fastafmt_the_record.
void fastafmt_free_record1(void);
void fastafmt_free_record2(void);
