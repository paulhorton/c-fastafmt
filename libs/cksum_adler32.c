/*
 *  Adler-32 checksum
 *
 *  Maintainer:  Paul Horton
 *
 *  Implementation adapted from source found on Wikipedia, 20221017.
 *
 */
#include "cksum_adler32.h"


uint32_t cksum_adler32(  const unsigned char* data,  size_t len  ){
    uint32_t a = 1, b = 0;

    const unsigned char* dataEnd=  data + len;

    while(  data < dataEnd  ){
      a=  (a + *data++) % 65521;
      b=  (a +  b     ) % 65521;
    }

    return (b << 16) | a;
}
