/*
 *  Author: Paul Horton
 *  Copyright: Paul Horton 2022, All rights reserved.
 *  Created: 20221107
 *  Updated: 20221107
 *
 *  License:  GNU General Public License GPLv3
 *
 *  Description:
 *      Functions for mapping between sequence bases to serial numbers
 *      For example, mapping  ([aA],[cC],[gG],[tT]) <--> (0,1,2,3).
 *
 *  Limitations:
 *      Case sensitive mappings are currently not supported.
 */
#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

//  Conceptually the constants below should be defined as  const baseSN_t DNA_baseSN_numBases
//  But in practice using #define is more convenient,
//    For example when using as the size of an array:    baseSN_t counts[DNA_baseSN_numBases];
#define DNA_baseSN_numBases    4//  |{a,c,g,t}|
#define DNA_baseSN_fin         3//  Maximum serial number of normal base {a,c,g,t}
#define DNA_baseSN_nonbase   126//  Represents 'n'
#define DNA_baseSN_seqEnd    127//  Can be used as sequence terminal sentinel (哨符值)

typedef  uint8_t  baseSN_t; // type of base(鹼基or殘基) after mapping {[Aa],[Cc],[Gg],[Tt]} to 0,1,2,3


// Return true iff b is the serial number of a valid DNA base.
bool DNA_baseSN_baseP(  baseSN_t b  );

// Return the (lower case) character corresponding to DNA serial number b.
char char_of_DNAbaseSN(  baseSN_t b  );

baseSN_t baseSN_of_DNAchar(  char c  );

baseSN_t baseSNcompl_of_baseSN(  baseSN_t b  );

bool baseSNs_complP(  baseSN_t b1, baseSN_t b2  );


/* ──────────  Math functions  ────────── */

// Return base serial number B raised to the POWth power,
// and return that as a size_t number.
// Warning; no check is made for overflow.
size_t baseSN_pow( baseSN_t b, size_t pow );
