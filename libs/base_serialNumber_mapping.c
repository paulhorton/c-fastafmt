/*
 *  Author: Paul Horton
 *  Copyright: Paul Horton 2022, All rights reserved.
 *  Created: 20221108
 *  Updated: 20221109
 *
 *  License:  GNU General Public License GPLv3
 *
 *  Description: See header file.
 */
#include "assert.h"
#include "base_serialNumber_mapping.h"


// Shorter local names for convenience.
static const baseSN_t NON=  DNA_baseSN_nonbase;
static const baseSN_t END=  DNA_baseSN_seqEnd;

baseSN_t baseSN_of_DNAchar_ARRAY[256]=
  {END,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,
   NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,
   NON,  0,NON,  1,NON,NON,NON,  2,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,  3,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,
   NON,  0,NON,  1,NON,NON,NON,  2,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,  3,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,
   NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,
   NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,
   NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,
   NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON,NON
  };


bool DNA_baseSN_baseP(  baseSN_t b  ){
  return  b < DNA_baseSN_numBases;
}

char char_of_DNAbaseSN(  baseSN_t b  ){
  assert(  b < DNA_baseSN_numBases  );
  return  "acgt"[b];
}

baseSN_t baseSN_of_DNAchar( char c ){
  return  baseSN_of_DNAchar_ARRAY[c];
}

baseSN_t baseSNcompl_of_baseSN(  baseSN_t b  ){
  return DNA_baseSN_fin-b;
}

bool baseSNs_complP(  baseSN_t b1, baseSN_t b2  ){
  return  b1 == baseSNcompl_of_baseSN(b2);
}


// Utility functions
size_t baseSN_pow( baseSN_t b, size_t pow ){
  size_t retval= b;
  while( 0 < --pow ){
    retval*= b;
  }
  return retval;
}
