/*
 *  Author: Paul Horton
 *  Copyright: Paul Horton 2022, All rights reserved.
 *  Created: 20221007
 *  Updated: 20221007
 *  Description: See header file.
 */
#include <assert.h>
#include "fastafmt.h"


static struct fastafmt_record _rec1, _rec2;

struct fastafmt_record* fastafmt_record1=  &_rec1;
struct fastafmt_record* fastafmt_record2=  &_rec2;

size_t fastafmt_num_records_loaded= 0;

static const size_t headBufInitSize=  0x100;

static size_t seqBufInitSize=  0x10000000;

static char*  curLineBuf;
static size_t curLineBufSize=  0x1000;



/*  ────────────────────  Utility Functions  ────────────────────  */
static size_t size_t_max2_(  size_t val1, size_t val2  ){
  return   (val1 >= val2)?  val1  : val2;
}

static size_t size_t_min2_(  size_t val1, size_t val2  ){
  return   (val1 <= val2)?  val1  : val2;
}


__attribute__((noreturn))
static void die_(  const char* message  ){
  fprintf(  stdout,   "(stdout) fastafmt error: %s\n", message  );
  fprintf(  stderr,   "(stderr) fastafmt error: %s\n", message  );
  exit( 64 );
}



/*  ────────────────────  Record Name Related  ────────────────────  */

//  Find name in rec->head and copy it into rec->name
//  Assumes rec->head already computed,
//  but rec->name may not even be allocated yet.
size_t fastafmt_name_interval(   const char** beg,  const char** end,
                                 const char* const head  ){

  *beg= head;
  for(  *beg=  head;
        **beg == ' '  ||  **beg == '\t';
        ++*beg  );

  *end= *beg;

  if(  **beg == '\0'  )    return 0;

  for(  ;
        **end != ' '  &&  **end != '\t';
        ++*end  );

  return  *end-*beg;
}


size_t fastafmt_copy_name(  char* const nameBuf,  size_t bufSize,
                            const char* const head  ){

  if(  !bufSize  )    die_(  "fastafmt_copy_name called with bufSize=0"  );

  const char  *beg, *end;
  fastafmt_name_interval( &beg, &end, head );

  if(  end - beg  >=  bufSize  ){
    strncpy(  nameBuf, beg, bufSize-1  );
    nameBuf[ bufSize-1]=  '\0';
    return  bufSize;
  }

  strncpy(  nameBuf, beg, end-beg  );
  nameBuf[ end-beg ]=  '\0';
  return  end - beg;
}


size_t fastafmt_copy_record1_name(  char* const nameBuf,  size_t bufSize  ){
  return  fastafmt_copy_name(  nameBuf, bufSize, fastafmt_record1->head  );
}

size_t fastafmt_copy_record2_name(  char* const nameBuf,  size_t bufSize  ){
  return  fastafmt_copy_name(  nameBuf, bufSize, fastafmt_record2->head  );
}



//  Read record in from input stream ifp and set fields of rec accordingly.
static int record_load_(  struct fastafmt_record* rec, FILE* ifp  ){

  if(  !rec->head  ){
    rec->head=  (char*) malloc( rec->headBufSize= headBufInitSize );
  }
  if(  !rec-> seq  ){
    rec-> seq=  (char*) malloc( rec-> seqBufSize= seqBufInitSize );
  }
  if(  !curLineBuf ){   curLineBuf= (char*) malloc(curLineBufSize);  }


  ssize_t numBytesRead;
  int firstCharInLine;

  /* ─────────────────────────  Read Head Line  ───────────────────────── */
 READ_CHAR1_OF_NEXT_HEAD_LINE:
  firstCharInLine=  getc( ifp );

  switch(  firstCharInLine  ){
  case  EOF:
    return 0; //  No record read                                        EXIT|
  case  '\n':
    goto  READ_CHAR1_OF_NEXT_HEAD_LINE;
  case  '#':
    getline(  &curLineBuf, &curLineBufSize, ifp  );   //  To advance past current line
    goto  READ_CHAR1_OF_NEXT_HEAD_LINE;
  case  '>':
    numBytesRead=   getline(  &rec->head, &rec->headBufSize, ifp  );
    char* prev=  rec->head + numBytesRead - 1;    if( *prev == '\n' )  *prev = '\0';//   Chomp.
    break;
  case '@':
    die_(  "'@' encountered.  Input might a fastq file, but that format is not yet supported"  );
  default:
    die_(  "when calling fastamt_record_read, input stream appeared to point to the middle of a line."  );
  }



  /* ─────────────────────────  Load Sequence  ───────────────────────── */
  ++fastafmt_num_records_loaded;
  char* curSeqEnd=  rec->seq;

  for( ;; ){

    // Read first character of sequence line, skipping comment lines and empty lines if present
    READ_CHAR1_OF_NEXT_SEQ_LINE:
    firstCharInLine=  getc( ifp );

    switch(  firstCharInLine  ){
    case  EOF:
      rec->seqLen=  curSeqEnd - rec->seq;
      if(  rec->seqLen  &&  rec->seq[rec->seqLen-1] == '*'  )  rec->seq[--rec->seqLen]= '\0';
      return 1; //  Rehd final record in stream.                         //EXIT
    case  '\n':
      goto  READ_CHAR1_OF_NEXT_SEQ_LINE;
    case  '#':
      getline(  &curLineBuf, &curLineBufSize, ifp  );   //  To advance past current line
      goto  READ_CHAR1_OF_NEXT_SEQ_LINE;
    case  '>':
      ungetc(  firstCharInLine, ifp  );
      rec->seqLen=  curSeqEnd - rec->seq;
      if(  rec->seqLen  &&  rec->seq[rec->seqLen-1] == '*'  )  rec->seq[--rec->seqLen]= '\0';
      return 2; //  Rehd record, but not the final record.               //EXIT
    }

    *curSeqEnd++= (char) firstCharInLine;


    // Read the second and following characters of the current line
    numBytesRead=  getline(  &curLineBuf, &curLineBufSize, ifp  );

    if(  numBytesRead > 0  &&  *(curLineBuf + numBytesRead-1) == '\n'  ){
      --numBytesRead;//  ignore \n before string terminating \0
    }

    char* seqBufMemoryEnd=  rec->seq + rec->seqBufSize;
    if(  seqBufMemoryEnd  <=  curSeqEnd + numBytesRead  ){
      //  Allocate new seq buffer and copy sequence as needed.
      size_t newBufSize=   rec->seqBufSize  + size_t_max2_( numBytesRead, rec->seqBufSize );

      char* newBuf=  malloc( newBufSize );
      memcpy( newBuf, rec->seq, rec->seqBufSize );
      curSeqEnd +=  newBuf - rec->seq;

      free(  rec->seq  );

      rec->seq=  newBuf;
      rec->seqBufSize=  newBufSize;
    }

    memcpy(   (void*) curSeqEnd,  (void*) curLineBuf,  numBytesRead   );
    curSeqEnd += numBytesRead;
    *curSeqEnd=  '\0';
  }
}


int fastafmt_load_record1(  FILE* ifp  ){
  return  record_load_(  fastafmt_record1, ifp  );
}

int fastafmt_load_record2(  FILE* ifp  ){
  return  record_load_(  fastafmt_record2, ifp  );
}

int fastafmt_load_record(  struct fastafmt_record* record1_or_2,  FILE* ifp  ){
  if(  record1_or_2 == fastafmt_record1  ||
       record1_or_2 == fastafmt_record2
       ){
    return record_load_(  record1_or_2,  ifp  );
  }

  if(  !record1_or_2  )   die_(  "fastafmt_load_record called with NULL record1_or_2 pointer"  );

  die_(  "fastafmt_load_record called with record pointer pointing to somewhere other than &fastafmt_record1 or &fastafmt_record2"  );
}



/* ─────────────────────────  Write Record  ───────────────────────── */

//  Write line to ofp without flushing.
void fastafmt_write_line_(  const char* src,  size_t numBytes,  FILE* ofp  ){

  if(   numBytes  !=  fwrite( src, 1, numBytes, ofp )){
    die_(  "failure when trying to output head of a record."  );
  }

  if(  1 !=  fwrite( "\n", 1, 1, ofp  )  ){
      die_(  "failure when trying to output head of a record."  );
  }
}


static void fastafmt_write_(  const char* head,
                              const char* seq,  size_t seqLen,
                              FILE* ofp,  size_t seqLineLen
                              ){

  if(  !head  )   die_(  "Tried to output record with head=NULL"  );
  if(  !seq   )   die_(  "Tried to output record with  seq=NULL"  );

  fastafmt_write_line_(  head,  strlen(head),  ofp  );

  const char* seqEnd=  seq + seqLen;
  for(  const char* seqCur= seq;
        seqCur < seqEnd;
        seqCur += seqLineLen  ){
    size_t numBytesToWrite=   size_t_min2_(  seqLen,  seqEnd - seqCur  );
    fastafmt_write_line_(  seqCur,  numBytesToWrite,  ofp  );
  }

  fflush( ofp );
}


void fastafmt_write(  const char* head,  const char* seq,
                      FILE* ofp,  size_t seqLineLen  ){
  fastafmt_write_(  head, seq, strlen(seq), ofp, seqLineLen  );
}


void fastafmt_write_record1(  FILE* ofp,  size_t seqLineLen  ){
  fastafmt_write_(  fastafmt_record1->head,
                    fastafmt_record1->seq,
                    fastafmt_record1->seqLen,
                    ofp, seqLineLen  );
}

void fastafmt_write_record2(  FILE* ofp,  size_t seqLineLen  ){
  fastafmt_write_(  fastafmt_record2->head,
                    fastafmt_record2->seq,
                    fastafmt_record2->seqLen,
                    ofp, seqLineLen  );
}




/* ─────────────────────────  Free Memory  ───────────────────────── */

static void record_free_(  struct fastafmt_record* rec  ){
  free( rec->head  );   rec->head=  NULL;
  free( rec->seq   );   rec->seq=   NULL;
  free( curLineBuf );  curLineBuf=  NULL;
}


void fastafmt_free_record1(void){  record_free_( fastafmt_record1 );   }
void fastafmt_free_record2(void){  record_free_( fastafmt_record2 );   }
