/*
 *  Header for adler32 cksum implementation.
 *  See .c file for acknowledgements.
 */
#pragma once
#include <stddef.h>
#include <stdint.h>


uint32_t cksum_adler32(  const unsigned char* data,  size_t len  );
