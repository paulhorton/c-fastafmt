# C-fastafmt -- C Library for reading fastafmt files

## Basic Usage
    #include "libs/fastafmt.h"

    // fastafmt_record1 is a global variable,
    // declared in fastafmt.h  and defined in fastafmt.c
    struct fastafmt_record* rec=  fastafmt_record1;

    //  Each call to fastafmt_load_record1 reads a record from stdin
    //  overwriting the previous contents of fastafmt_record1
    //  fastafmt_load_record1 returns false when all of stdin has been read in.
    while(  fastafmt_load_record1( stdin )  ){
      printf(  "head is '%s'\n"   , rec->head    );
      printf(  "seq len = %zu\n"  , rec->seqLen  );
      printf(  "seq is:%s\n"      , rec->seq     );
    }


## Learn More
* Look at libs/fastafmt.h to see what functions are provided.

* Look at executable programs under the src/ directory


## Regression testing
Some information helpful for *manual* regression testing is
provided in the file: regression\_testing/regression\_testing\_info.txt


### Author
Paul Horton.  Copyright 2022.
